import javafx.animation.AnimationTimer;
import model.Model;

public class Timer extends AnimationTimer {

    private Graphics graphics;
    private Model model;


    public Timer(Graphics graphics, Model model) {
        this.graphics = graphics;
        this.model = model;

    }

    private long previousTime = -1;

    @Override
    public void handle(long nowNano) {

        long nowMillis = nowNano / 1000000;
        long elapsedTime;
        if (previousTime == -1) {
            elapsedTime = 0;
        } else {
            elapsedTime = nowMillis - previousTime;
        }
        previousTime = nowMillis;

        graphics.draw();
        model.update(elapsedTime);
    }
}
