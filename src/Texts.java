import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class Texts {

    private GraphicsContext gc;



    public Texts(GraphicsContext gc) {
        this.gc = gc;
    }
    public Font loadFonts() {
        return Font.loadFont(getClass().getResourceAsStream("src/resources/fonts/mangat.ttf"), 80);
    }

    public void createText(String message, int x, int y, float z) {
        loadFonts();
        Text text = new Text();
        text.setFont(Font.font("Manga Temple", z));
        text.setFill(Color.LAVENDER);
        text.setText(message);
        text.setX(x);
        text.setY(y);
    }

}



