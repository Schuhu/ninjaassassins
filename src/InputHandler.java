import javafx.scene.input.KeyCode;
import model.Model;

public class InputHandler {

    private Model model;

    public InputHandler(Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyCode keyCode) {
        moveSakura(keyCode);
        moveKatsumi(keyCode);

        if (keyCode == KeyCode.SPACE && !model.getSakura().isKO()) {
            if(model.getSakura().isCanAttack()) {
                model.getSakura().attack(model.getKatsumi());
                model.getSakura().setCanAttack(false);
            }
        }
        if (keyCode == KeyCode.INSERT && !model.getKatsumi().isKO()) {
            if(model.getKatsumi().isCanAttack() && !model.getKatsumi().isKO()) {
                model.getKatsumi().attack(model.getSakura());
                model.getKatsumi().setCanAttack(false);
            }
        }

        if (keyCode == KeyCode.Q && !model.getSakura().isKO()) {
            model.getSakura().throwDagger(model.getSakurasDagger());
        }
        if (keyCode == KeyCode.F && model.getSakura().isCanSuperAttack() && model.getSakura().isThrowing()){
            model.getSakura().superAttack(model.getSakurasSuperAttack());
            model.getSakura().setCanSuperAttack(false);
        }
        if (keyCode == KeyCode.ENTER && model.getKatsumi().isCanSuperAttack() && model.getKatsumi().isThrowing()){
            model.getKatsumi().superAttack(model.getKatsumisSuperAttack());
            model.getKatsumi().setCanSuperAttack(false);
        }

        if (keyCode == KeyCode.PAGE_UP && !model.getKatsumi().isKO()) {
            model.getKatsumi().throwDagger(model.getKatsumisDagger());
        }
        if (keyCode == KeyCode.Y && model.isEndOfTheGame()) {
            model.setEndOfTheGame(false);
            model.setRound3(false);
            model.getSakura().setRoundsWon(0);
            model.getKatsumi().setRoundsWon(0);
            model.setStartOfTheGame(true);

        }
    }

    public void onKeyReleased(KeyCode keyCode) {
        if (keyCode == KeyCode.SPACE) {
            model.getSakura().setCanAttack(false);
        }
        if (keyCode == KeyCode.INSERT) {
            model.getKatsumi().setCanAttack(false);
        }
        if (keyCode == KeyCode.D) {
            model.getSakura().setIsMoving(false);
            model.getSakura().setMovingRight(false);
            model.getSakura().moveRight();
        }
        if (keyCode == KeyCode.LEFT) {
            model.getKatsumi().setIsMoving(false);
            model.getKatsumi().setMovingLeft(false);
            model.getKatsumi().moveLeft();
        }
        if (keyCode == KeyCode.A) {
            model.getSakura().setMovingLeft(false);
            model.getSakura().moveLeft();
        }
        if (keyCode == KeyCode.RIGHT) {
            model.getKatsumi().setMovingRight(false);
            model.getKatsumi().moveRight();
        }
        if (keyCode == KeyCode.S) {
            model.getSakura().setDucked(false);
        }
        if (keyCode == KeyCode.DOWN) {
            model.getKatsumi().setDucked(false);
        }
    }

    //All key actions for moving left and right, jumping and ducking.

    public void moveSakura(KeyCode keyCode) {
        if (keyCode == KeyCode.W  &&  !model.getSakura().isKO()) {
            model.getSakura().jump();
            model.getSakura().doubleJump();
        }
        if (keyCode == KeyCode.A && !model.getSakura().isKO()) {
            model.getSakura().setMovingLeft(true);
        }
        if (keyCode == KeyCode.D && !model.getSakura().isKO()) {
            model.getSakura().setMovingRight(true);
            model.getSakura().setIsMoving(true);
        }
        if (keyCode == KeyCode.S && !model.getSakura().isKO()) {
            if(!model.getSakura().getIsFalling() && !model.getSakura().isMovingRight() && !model.getSakura().isMovingLeft()) {
                model.getSakura().setDucked(true);
            }
        }
    }

    public void moveKatsumi(KeyCode keyCode) {
        if (keyCode == KeyCode.UP && !model.getKatsumi().isKO()) {
            model.getKatsumi().jump();
            model.getKatsumi().doubleJump();
        }
        if (keyCode == KeyCode.LEFT && !model.getKatsumi().isKO()) {
            model.getKatsumi().setMovingLeft(true);
            model.getKatsumi().setIsMoving(true);
        }
        if (keyCode == KeyCode.RIGHT && !model.getKatsumi().isKO()) {
            model.getKatsumi().setMovingRight(true);
        }
        if (keyCode == KeyCode.DOWN && !model.getKatsumi().isKO()) {
            if(!model.getKatsumi().getIsFalling() && !model.getKatsumi().isMovingRight() && !model.getKatsumi().isMovingLeft()) {
                model.getKatsumi().setDucked(true);
            }
        }
    }
}

