import javafx.scene.image.Image;
import javafx.scene.canvas.GraphicsContext;

import javafx.scene.paint.Color;
import model.Model;


import java.io.FileInputStream;
import java.io.FileNotFoundException;


public class Graphics {
    private GraphicsContext gc;
    private Model model;




    public Graphics (GraphicsContext gc, Model model) throws FileNotFoundException {
        this.gc = gc;
        this.model = model;
    }
    //Instantiate Environment-Images
    private Image imageBackground = new Image(new FileInputStream("src/resources/environment/JapanBackgrounds.jpg"), 0, 1200, true, true);
    private Image imagePlatform = new Image(new FileInputStream("src/resources/environment/platformPack_tile034.png"));
    private Image imagePlatformSupport = new Image(new FileInputStream("src/resources/environment/platformPack_tile038.png"));
    private Image flashHitGif = new Image(new FileInputStream("src/resources/environment/FlashHit.gif"), 0, 180, true, true);
    private Image katsumiWinsScreen = new Image(new FileInputStream("src/resources/environment/KatsumiWins.jpg"), 0, 1200, true, true);
    private Image sakuraWinsScreen = new Image(new FileInputStream("src/resources/environment/SakuraWins.jpg"), 0, 1200, true, true);
    private Image firstBackgroundWithText = new Image(new FileInputStream("src/resources/environment/FirstBackgroundWithText.png"), 0, 1200, true, true);
    private Image secondBackground = new Image(new FileInputStream("src/resources/environment/SecondBackground.jpg"), 0, 1200, true, true);
    private Image thirdBackground = new Image(new FileInputStream("src/resources/environment/ThirdBackground.jpg"), 0, 1200, true, true);
    private Image round1 = new Image(new FileInputStream("src/resources/environment/round1.png"), 0, 0, true, true);
    private Image round2 = new Image(new FileInputStream("src/resources/environment/round2.png"), 0, 0, true, true);
    private Image finalRound = new Image(new FileInputStream("src/resources/environment/finalRound.png"), 0, 0, true, true);



    //Instantiate Katsumi-Images
    private Image katsumiIdleGif = new Image(new FileInputStream("src/resources/katsumi/KatsumiIdle.gif"));
    private Image katsumiAttackGif = new Image(new FileInputStream("src/resources/katsumi/KatsumiAttack.gif"));
    private Image katsumiRunGif = new Image(new FileInputStream("src/resources/katsumi/KatsumiRun.gif"));
    private Image katsumiFallingGif = new Image(new FileInputStream("src/resources/katsumi/KatsumiFalling.gif"));
    private Image katsumiKO = new Image(new FileInputStream("src/resources/katsumi/KatsumiKO.gif"));
    private Image katsumiJumpAttackGif = new Image(new FileInputStream("src/resources/katsumi/KatsumiJumpAttack.gif"));
    private Image katsumiThrowGif = new Image(new FileInputStream("src/resources/katsumi/KatsumiThrow.gif"));
    private Image katsumiJumpGif = new Image(new FileInputStream("src/resources/katsumi/KatsumiJump.gif"));
    private Image katsumiSlideGif = new Image(new FileInputStream("src/resources/katsumi/KatsumiSlide.gif"));
    private Image katsumisDagger = new Image(new FileInputStream("src/resources/katsumi/KatsumisDagger.png"));
    private Image katsumisSuperAttack = new Image(new FileInputStream("src/resources/katsumi/superAttackKatsumi.gif"));


    //Instantiate Sakura-Images
    private Image sakuraIdleGif = new Image(new FileInputStream("src/resources/sakura/SakuraIdle.gif"), 0, 440, true, true);
    private Image sakuraAttackGif = new Image(new FileInputStream("src/resources/sakura/SakuraAttack.gif"), 0, 480, true, true);
    private Image sakuraSprintGif = new Image(new FileInputStream("src/resources/sakura/SakuraRunning.gif"), 0, 470, true, true);
    private Image sakuraJumpGif = new Image(new FileInputStream("src/resources/sakura/SakuraJump.gif"), 0, 460, true, true);
    private Image sakuraKO = new Image(new FileInputStream("src/resources/sakura/SakuraKO.gif"), 0, 440, true, true);
    private Image sakuraJumpAttackGif = new Image(new FileInputStream("src/resources/sakura/SakuraJumpAttack.gif"), 0, 460, true, true);
    private Image sakuraSlidekGif = new Image(new FileInputStream("src/resources/sakura/SakuraSlide.gif"), 0, 350, true, true);
    private Image sakuraFallingGif = new Image(new FileInputStream("src/resources/sakura/SakuraFalling.gif"), 0, 440, true, true);
    private Image sakuraThrowGif = new Image(new FileInputStream("src/resources/sakura/SakuraThrow.gif"), 0, 440, true, true);
    private Image sakurasDagger = new Image(new FileInputStream("src/resources/sakura/SakurasDagger.png"), 0, 30, true, true);
    private Image sakurasSuperAttack = new Image(new FileInputStream("src/resources/sakura/superAttackSakura.gif"), 0, 0, true, true);





    public void draw() {

        if (model.isStartOfTheGame()) {
            gc.drawImage(firstBackgroundWithText, 0, 0);
        } else if (model.isEndOfTheGame()) {
            if(model.getSakura().getRoundsWon() == 2) {
                gc.drawImage(sakuraWinsScreen, 0, 0);
            }
            if(model.getKatsumi().getRoundsWon() == 2) {
                gc.drawImage(katsumiWinsScreen, 0, 0);
            }

        }
        else if (!model.getSakura().isKO() || !model.getKatsumi().isKO()) {

            gc.clearRect(0, 0, model.WIDTH, model.HEIGHT);
            drawBackground();
            drawDaggers();
            drawSuperAttacks();
            drawSakura();
            drawKatsumi();
            drawGround();
            drawHealthBars();


        }
    }

    public void drawBackground() {
        if (model.isRound1()) {
            gc.drawImage(imageBackground, 0, 0);
            gc.drawImage(round1, 780, 50);
        }
        else if (model.isRound2()) {
            gc.drawImage(secondBackground, 0, 0);
            gc.drawImage(round2, 780, 50);

        }
        else if (model.isRound3()) {
            gc.drawImage(thirdBackground,0,0);
            gc.drawImage(finalRound, 680, 50);

        }
        else  gc.drawImage(imageBackground, 0, 0);
    }

    public void drawGround() {
        for (int i = 0; i <= model.WIDTH; i += 60) {
            gc.drawImage(imagePlatform, i, 940);
        }
        gc.drawImage(imagePlatformSupport, 60, 1000);
        gc.drawImage(imagePlatformSupport, 1800, 1000);
    }
    public void drawDaggers() {
        gc.drawImage(sakurasDagger, model.getSakurasDagger().getX(), model.getSakurasDagger().getY()+200);
        gc.drawImage(katsumisDagger, model.getKatsumisDagger().getX(), model.getKatsumisDagger().getY()+200);
    }
    public void drawSuperAttacks() {
        gc.drawImage(sakurasSuperAttack, model.getSakurasSuperAttack().getX(), model.getSakurasSuperAttack().getY()+200);
        gc.drawImage(katsumisSuperAttack, model.getKatsumisSuperAttack().getX(), model.getKatsumisSuperAttack().getY()+200);

    }

    public void drawSakura() {

        if (model.isStartOfTheRound()) {
            gc.drawImage(sakuraFallingGif, model.getSakura().getX(), model.getSakura().getY());
        }
        else if (model.getSakura().isKO()) {
            gc.drawImage(sakuraKO, model.getSakura().getX(), model.getSakura().getY()+50);
        }
        else if(model.getSakura().getIsFalling()) {
            if(!model.getSakura().isCanAttack()){
                gc.drawImage(sakuraJumpAttackGif, model.getSakura().getX(), model.getSakura().getY());
            }
            else if (model.getSakura().getSpeedY() < -15 && model.getSakura().getY() < 300) {
                gc.drawImage(flashHitGif, model.getSakura().getX()+50, model.getSakura().getY()+350);
                gc.drawImage(sakuraJumpGif, model.getSakura().getX(), model.getSakura().getY());
            }
            else
                gc.drawImage(sakuraJumpGif, model.getSakura().getX(), model.getSakura().getY());
        }
        else if (!(model.getSakura().isCanAttack())) {
            gc.drawImage(sakuraAttackGif, model.getSakura().getX(), model.getSakura().getY());
        } else if(model.getSakura().getIsMoving()) {
            gc.drawImage(sakuraSprintGif, model.getSakura().getX(), model.getSakura().getY());
        } else if(model.getSakura().isDucked()){
            gc.drawImage(sakuraSlidekGif, model.getSakura().getX(), model.getSakura().getY()+100);
        } else if(model.getSakura().isThrowing()) {
            gc.drawImage(sakuraThrowGif, model.getSakura().getX(), model.getSakura().getY());
        }
        else gc.drawImage(sakuraIdleGif, model.getSakura().getX(), model.getSakura().getY());

    }

     public void drawKatsumi() {

         if (model.isStartOfTheRound()) {
             gc.drawImage(katsumiFallingGif, model.getKatsumi().getX() - 200, model.getKatsumi().getY());
         } else if (model.getKatsumi().isKO()) {
             gc.drawImage(katsumiKO, model.getKatsumi().getX(), model.getKatsumi().getY());
         } else if (model.getKatsumi().getIsFalling()) {
             if (!model.getKatsumi().isCanAttack()) {
                 gc.drawImage(katsumiJumpAttackGif, model.getKatsumi().getX(), model.getKatsumi().getY());
             } else if (model.getKatsumi().getSpeedY() < -15 && model.getKatsumi().getY() < 300) {
                 gc.drawImage(flashHitGif, model.getKatsumi().getX() + 50, model.getKatsumi().getY() + 350);
                 gc.drawImage(katsumiJumpGif, model.getKatsumi().getX(), model.getKatsumi().getY());
             } else
                 gc.drawImage(katsumiJumpGif, model.getKatsumi().getX(), model.getKatsumi().getY());
         }
         else if (!(model.getKatsumi().isCanAttack())) {
             gc.drawImage(katsumiAttackGif, model.getKatsumi().getX()-200, model.getKatsumi().getY());
         } else if(model.getKatsumi().getIsMoving()) {
             gc.drawImage(katsumiRunGif, model.getKatsumi().getX(), model.getKatsumi().getY());
         } else if(model.getKatsumi().isDucked()){
             gc.drawImage(katsumiSlideGif, model.getKatsumi().getX(), model.getKatsumi().getY()+100);
         } else if(model.getKatsumi().isThrowing()) {
             gc.drawImage(katsumiThrowGif, model.getKatsumi().getX(), model.getKatsumi().getY());
         }
         else gc.drawImage(katsumiIdleGif, model.getKatsumi().getX(), model.getKatsumi().getY());

     }

    public void drawHealthBars() {
        Color fontColor = new Color(0.54, 0.31, 0.31, 0.8);
        Color fontColor2 = new Color(0.54, 0.31, 0.31, 0.2);
        gc.strokeRoundRect(50, 50, 2.5*model.getSakura().getLifePoints(), 30, 10,10);
        gc.setFill(fontColor);
        gc.fillRoundRect(50, 50, 2.5*model.getSakura().getLifePoints(), 30, 10,10);
        gc.strokeRoundRect(1900-2.5*model.getKatsumi().getLifePoints() , 50, 2.5*model.getKatsumi().getLifePoints(), 30, 10,10);
        gc.fillRoundRect(1900-2.5*model.getKatsumi().getLifePoints(), 50, 2.5*model.getKatsumi().getLifePoints(), 30, 10,10);

        gc.setFill(fontColor2);
        gc.fillRoundRect(1650, 50, 250, 30, 10,10);
        gc.fillRoundRect(50, 50, 250, 30, 10,10);
    }
}
