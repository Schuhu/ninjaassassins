package model;

public class SuperAttack {

    private int x;
    private int y;
    private boolean thrown;

    public SuperAttack(int x, int y) {
        this.x = x;
        this.y = y;
        this.thrown = true;
    }

    public void move(int dx) {
        this.x += dx;
    }

    public void doDamge(Player player) {
        if(Math.abs(this.getX() - player.getX()) >= 0 && Math.abs(this.getX() -player.getX()) <=50
                && Math.abs(this.getY() -player.getY()) >= 0 && Math.abs(this.getY() -player.getY()) <= 50
                && !player.isDucked())
        {
            int life = player.getLifePoints();
            life -= 30;
            player.setLifePoints(life);
            player.setHitByDagger(true);
            this.setY(2000);
        }
    }

    //Getter und Setter
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public boolean isThrown() {
        return thrown;
    }

    public void setThrown(boolean thrown) {
        this.thrown = thrown;
    }
}

