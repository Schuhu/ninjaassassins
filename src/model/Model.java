package model;

public class Model {


    private Player sakura;
    private Player katsumi;
    private Dagger sakurasDagger;
    private Dagger katsumisDagger;
    private SuperAttack sakurasSuperAttack;
    private SuperAttack katsumisSuperAttack;


    private boolean startOfTheGame = true;
    private boolean startOfTheRound = false;
    private boolean round1 = false;
    private boolean round2 = false;
    private boolean round3 = false;
    private boolean endOfTheGame = false;



    private long secondsCounter = 3000;
    private long secondsCounter2 = 3000;
    private long secondsCounter3 = 10000;
    private long secondsCounter4 = 10000;
    private long counter = 5000;
    private long roundsCounter = 1000;

    public final double WIDTH = 1920;
    public final double HEIGHT = 1080;

    public Model() {
        this.sakura = new Player(0, -500);
        this.katsumi = new Player(1650, -500);
        this.sakurasDagger = new Dagger(5000, 5000);
        this.katsumisDagger = new Dagger(5000,5000);
        this.sakurasSuperAttack = new SuperAttack(5000, 5000);
        this.katsumisSuperAttack = new SuperAttack(5000, 5000);
    }

    public Player getSakura() {
        return sakura;
    }
    public Player getKatsumi() {
        return katsumi;
    }
    public Dagger getSakurasDagger() { return sakurasDagger; }
    public Dagger getKatsumisDagger() { return katsumisDagger; }
    public SuperAttack getSakurasSuperAttack() {return sakurasSuperAttack;}
    public SuperAttack getKatsumisSuperAttack() {return katsumisSuperAttack;}

    public void update(long elapsedTime) {

     if (startOfTheGame) {


         counter -= elapsedTime;
         if(counter <= 0) {
             setStartOfTheGame(false);
             setStartOfTheRound(true);
         }
     }

        //Start-Conditions are defined and overwrite everything else.
     else if (startOfTheRound)  {

         sakura.move(0, 3);
         katsumi.move(0, 3);

         if (sakura.getY()> 450 && katsumi.getY()> 450) {
             setStartOfTheRound(false);
             if (sakura.getRoundsWon() + katsumi.getRoundsWon() == 0)  {setRound1(true);}
             if (sakura.getRoundsWon() + katsumi.getRoundsWon() == 1)  {
                 setRound1(false);
                 setRound2(true);}
             if (sakura.getRoundsWon() + katsumi.getRoundsWon() == 2)  {
                 setRound2(false);
                 setRound3(true);}
         }
     }
     //All other update functions are being checked.
     else if (round1 || round2 || round3) {

         //Gives both players a gravity, which is increasing over time. Allows for dynamic falling.
         sakura.setSpeedY(sakura.getSpeedY() + 0.5);
         if (sakura.getY() >= 500 && sakura.getSpeedY() > 0) {
             sakura.setSpeedY(0);
             sakura.setAllowedToJump(true);
             sakura.setCanDoubleJump(true);
             sakura.setY(500);
         }

         katsumi.setSpeedY(katsumi.getSpeedY() + 0.5);
         if (katsumi.getY() >= 500 && katsumi.getSpeedY() > 0) {
             katsumi.setSpeedY(0);
             katsumi.setAllowedToJump(true);
             katsumi.setCanDoubleJump(true);
             katsumi.setY(500);
         }

         //Defines an attack timer for both players.
         secondsCounter -= elapsedTime;
         if (secondsCounter <= 0) {
             sakura.setCanAttack(true);
             secondsCounter = 1500;
         }
         secondsCounter2 -= elapsedTime;
         if (secondsCounter2 <= 0) {
             katsumi.setCanAttack(true);
             secondsCounter2 = 1500;
         }
         //Defines a superAttack timer for both players.
         secondsCounter3 -= elapsedTime;
         if (secondsCounter3 <= 0) {
             sakura.setCanSuperAttack(true);
             secondsCounter3 = 10000;
         }
         secondsCounter4 -= elapsedTime;
         if (secondsCounter4 <= 0) {
             katsumi.setCanSuperAttack(true);
             secondsCounter4 = 10000;
         }


         //Defines the movement conditions for both players.
         sakura.move(0, (int) sakura.getSpeedY());
         if (sakura.isMovingRight() && ((sakura.getX() <= 1700 && (sakura.getX() + 200) < katsumi.getX()))) {
             sakura.move(15, 0);
         }
         if (sakura.isMovingLeft() && (sakura.getX() >= 0)) {
             sakura.move(-15, 0);
         }
         katsumi.move(0, (int) katsumi.getSpeedY());
         if (katsumi.isMovingRight() && ((katsumi.getX() <= 1700 ))) {
             katsumi.move(15, 0);
         }
         if (katsumi.isMovingLeft() && (katsumi.getX() >= 0 && (katsumi.getX() - 200) > sakura.getX())) {
             katsumi.move(-15, 0);
         }

         //Conditions for both daggers are checked and set.
         //if(sakurasDagger.isThrown()) {
             sakurasDagger.move(30);
             sakurasDagger.doDamge(katsumi);
         //}

         sakurasSuperAttack.move(25);
         sakurasSuperAttack.doDamge(katsumi);
         katsumisSuperAttack.move(-25);
         katsumisSuperAttack.doDamge(sakura);


         if(Math.abs(sakurasDagger.getX()-sakura.getX()) > 300){
             sakura.setThrowing(false);
         }

         katsumisDagger.move(-30);
         katsumisDagger.doDamge(sakura);
         if(Math.abs(katsumisDagger.getX()-katsumi.getX()) > 300){
             katsumi.setThrowing(false);
         }
         //Checks for end of rounds and of the game;

         if (sakura.isKO() || katsumi.isKO()) {

             roundsCounter -= elapsedTime;

             if (sakura.isKO() && roundsCounter <= 0) {
                 int wins = katsumi.getRoundsWon() + 1;
                 katsumi.setRoundsWon((wins));
                 sakura.setKO(false);
                 setStartOfTheRound(true);
                 sakura.setX(0);
                 sakura.setY(-500);
                 katsumi.setX(1650);
                 katsumi.setY(-500);
                 sakura.setLifePoints(100);
                 katsumi.setLifePoints(100);
                 roundsCounter = 1000;
             }
             else if (katsumi.isKO() && roundsCounter <= 0) {
                 int wins = sakura.getRoundsWon() + 1;
                 sakura.setRoundsWon(wins);
                 katsumi.setKO(false);
                 setStartOfTheRound(true);
                 sakura.setX(0);
                 sakura.setY(-500);
                 katsumi.setX(1650);
                 katsumi.setY(-500);
                 katsumi.setLifePoints(100);
                 sakura.setLifePoints(100);
                 roundsCounter = 1000;
             }
             if (sakura.getRoundsWon() == 2 || katsumi.getRoundsWon() == 2) {
                 setEndOfTheGame(true);
                 setRound2(false);
                 setRound3(false);
             }
         }



        //Checks for different individual booleans are performed.
         sakura.checkFalling();
         katsumi.checkFalling();
         sakura.checkKO();
         katsumi.checkKO();

     }
    }

    //Getter and setter.
    public boolean isStartOfTheRound() {
        return startOfTheRound;
    }

    private void setStartOfTheRound(boolean startOfTheRound) {
        this.startOfTheRound = startOfTheRound;
    }

    public boolean isStartOfTheGame() {
        return startOfTheGame;
    }

    public void setStartOfTheGame(boolean startOfTheGame) {
        this.startOfTheGame = startOfTheGame;
    }

    public boolean isRound1() {
        return round1;
    }

    public void setRound1(boolean round1) {
        this.round1 = round1;
    }

    public boolean isRound2() {
        return round2;
    }

    public void setRound2(boolean round2) {
        this.round2 = round2;
    }

    public boolean isRound3() {
        return round3;
    }

    public void setRound3(boolean round3) {
        this.round3 = round3;
    }

    public boolean isEndOfTheGame() {
        return endOfTheGame;
    }

    public void setEndOfTheGame(boolean endOfTheGame) {
        this.endOfTheGame = endOfTheGame;
    }
}

