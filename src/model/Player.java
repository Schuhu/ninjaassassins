package model;

public class Player {
    private int x;
    private int y;
    private int lifePoints;
    private boolean canAttack;
    private boolean canSuperAttack;
    private boolean isMoving;
    private boolean isFalling;
    private boolean KO;
    private boolean wasHitRecently;
    private double speedY;
    private double speedX;
    private boolean allowedToJump;
    private boolean movingRight;
    private boolean movingLeft;
    private boolean ducked;
    private boolean canDoubleJump;
    private boolean throwing;
    private boolean hitByDagger;
    private int roundsWon;


    public Player(int x, int y) {
        this.x = x;
        this.y = y;
        this.lifePoints = 100;
        this.canAttack = true;
        this.canSuperAttack = false;
        this.isMoving = false;
        this.isFalling = true;
        this.KO = false;
        this.wasHitRecently = false;
        this.speedY = 0;
        this.speedX = 0;
        this.allowedToJump = true;
        this.movingRight = false;
        this.movingLeft = false;
        this.ducked = false;
        this.canDoubleJump = false;
        this.throwing = false;
        this.hitByDagger = false;
        this.roundsWon = 0;
    }

    public void move(int dx, int dy) {
        this.x += dx;
        this.y += dy;
    }

    public void attack(Player player) {
        if(Math.abs(this.getX()-player.getX()) < 300 && Math.abs(this.getY()-player.getY()) < 300 && !(this.isKO())){
        int life = player.getLifePoints();
        life -= 20;
        player.setLifePoints(life);}
        System.out.println(player.getLifePoints());
    }

    public void throwDagger(Dagger dagger) {
        if(Math.abs(this.getX()-dagger.getX()) > 2000 && !this.isDucked()) {
            dagger.setX(this.getX());
            dagger.setY(this.getY());
            this.setThrowing(true);
        }
    }

    public void superAttack(SuperAttack superAttack) {
        if(Math.abs(this.getX()-superAttack.getX()) > 2000 && !this.isDucked()) {
            superAttack.setX(this.getX());
            superAttack.setY(this.getY());
            this.setThrowing(true);
        }
    }

    public void jump() {
        if (this.isAllowedToJump()) {
            this.setSpeedY(-19);
            this.setAllowedToJump(false);
        }
    }

    public void doubleJump(){
        if (this.isCanDoubleJump() && this.getSpeedY() > -4
                && this.getSpeedY() < 0) {
            this.setSpeedY(-19);
            this.setCanDoubleJump(false);
        }
    }

    public void moveRight() {
        if (this.isMovingRight()) {
            this.setSpeedX(15);
        } else this.setSpeedX(0);
    }

    public void moveLeft() {
        if (this.isMovingLeft()) {
            this.setSpeedX(-15);
        } else this.setSpeedX(0);
    }


    public void checkFalling() {
        if (this.getY() < 460) {
            this.setIsFalling(true);
        } else this.setIsFalling(false);
    }

    public void checkKO() {
        if (this.getLifePoints() <= 0) {
            this.setKO(true);
        }
    }
    public void setX(int x) { this.x = x; }

    public void setY(int y) {
        this.y = y;
    }

    //Getter und Setter
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getLifePoints() {
        return lifePoints;
    }

    public void setLifePoints(int lifePoints) {
        this.lifePoints = lifePoints;
    }

    public boolean isCanAttack() {
        return canAttack;
    }

    public void setCanAttack(boolean canAttack) {
        this.canAttack = canAttack;
    }

    public boolean getIsMoving() {
        return isMoving;
    }

    public void setIsMoving(boolean moving) {
        this.isMoving = moving;
    }

    public boolean getIsFalling() {
        return isFalling;
    }

    public void setIsFalling(boolean falling) {
        this.isFalling = falling;
    }

    public boolean isKO() {
        return KO;
    }

    public void setKO(boolean KO) {
        this.KO = KO;
    }

    public boolean isWasHitRecently() {
        return wasHitRecently;
    }

    public void setWasHitRecently(boolean wasHitRecently) {
        this.wasHitRecently = wasHitRecently;
    }

    public double getSpeedY() {
        return speedY;
    }

    public void setSpeedY(double speedY) {
        this.speedY = speedY;
    }

    public boolean isAllowedToJump() {
        return allowedToJump;
    }

    public void setAllowedToJump(boolean allowedToJump) {
        this.allowedToJump = allowedToJump;
    }

    public boolean isMovingRight() {
        return movingRight;
    }

    public void setMovingRight(boolean movingRight) {
        this.movingRight = movingRight;
    }

    public boolean isMovingLeft() {
        return movingLeft;
    }

    public void setMovingLeft(boolean movingLeft) {
        this.movingLeft = movingLeft;
    }

    public double getSpeedX() {
        return speedX;
    }

    public void setSpeedX(double speedX) {
        this.speedX = speedX;
    }

    public boolean isDucked() {
        return ducked;
    }

    public void setDucked(boolean ducked) {
        this.ducked = ducked;
    }

    public boolean isCanDoubleJump() {
        return canDoubleJump;
    }

    public void setCanDoubleJump(boolean canDoubleJump) {
        this.canDoubleJump = canDoubleJump;
    }

    public boolean isThrowing() {
        return throwing;
    }

    public void setThrowing(boolean throwing) {
        this.throwing = throwing;
    }

    public boolean isHitByDagger() { return hitByDagger; }

    public void setHitByDagger(boolean hitByDagger) { this.hitByDagger = hitByDagger; }

    public int getRoundsWon() {
        return roundsWon;
    }
    public void setRoundsWon(int roundsWon) {
        this.roundsWon = roundsWon;
    }

    public boolean isCanSuperAttack() {
        return canSuperAttack;
    }

    public void setCanSuperAttack(boolean canSuperAttack) {
        this.canSuperAttack = canSuperAttack;
    }
}
